import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { ApiService } from '../services/api.service';
import { SocketService } from '../services/socket/socket.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Tickers', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Tickers', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );

  symbols: [];

  ngOnInit() {
    this.getData();
    this.getTrades();
    this.connectSocket();
  }

  constructor(private breakpointObserver: BreakpointObserver, private api: ApiService, private io: SocketService) { }

  getData(): void {
    this.api.getSymbol().subscribe((symbol) => {
      console.log(symbol);
      this.symbols = symbol;
    });
  }

  getTicker(): void {
    this.api.getTicker('btcusd').subscribe((ticker) => {
      console.log(ticker.ask + ' ' + ticker.timestamp);
    });
  }

  getTrades(): void {
    this.api.getTrades('btcusd').subscribe((trade) => {
      trade.forEach((val) => {
        console.log(val.type);
      });
    }, (error) => {
      console.log(error);
    });
  }

  connectSocket(): void {
    let socket = this.io.connectSocket();
  }

}
