
export interface Stats {
    period: number;
    volume: string;
}
