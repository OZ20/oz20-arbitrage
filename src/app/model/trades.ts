
export interface Trades {
    timestamp: Date;
    tid: number;
    price: string;
    amount: string;
    exchange: string;
    type: string;
}
