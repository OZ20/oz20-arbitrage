import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ticker } from '../model/ticker';
import { Trades } from '../model/trades';
import { Stats } from '../model/stats';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getTicker(symbol: string): Observable<Ticker> {
    return this.http.get<Ticker>(proxyUrl + `/api.bitfinex.com/v1/pubticker/${symbol}`, httpOptions);
  }

  getSymbol(): Observable<[]> {
    return this.http.get<[]>(proxyUrl + `/api.bitfinex.com/v1/symbols`);
  }

  getStats(symbol: string): Observable<Stats> {
    return this.http.get<Stats>(proxyUrl + `/api.bitfinex.com/v1/stats/${symbol}`);
  }

  getTrades(symbol: string): Observable<Trades[]> {
    return this.http.get<Trades[]>(proxyUrl + `/api.bitfinex.com/v1/trades/${symbol}`);
  }
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

const proxyUrl = 'https://cors-anywhere.herokuapp.com';
