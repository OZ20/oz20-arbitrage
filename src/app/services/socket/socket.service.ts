import { Injectable } from '@angular/core';
import * as io from 'socket.io-client'
import { Observable,Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private apiUrl = 'http://localhost:3000';
  
  private socket;
  constructor() {}

  connectSocket(): void {
    this.socket = io(this.apiUrl);
    this.socket.on('connect', (socket) => {
      console.log('Connection successful made to: ' , socket.id);
    });
  }

}
